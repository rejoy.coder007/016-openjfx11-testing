package com.almasb.test;

import aa_config.Config_project;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;


/**
 * @author Almas Baimagambetov (almaslvl@gmail.com)
 */
public class FXApp extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {


        Parent root = (Parent)FXMLLoader.load(getClass().getClassLoader().getResource("./fxml/start.fxml"));
        Scene scene = new Scene(root, Config_project.START_WIDTH,Config_project.START_HEIGHT);
        primaryStage.setTitle(Config_project.COMPANY_NAME);
        primaryStage.setScene(scene);
        primaryStage.show();


 /*

            String javaVersion = System.getProperty("java.version");
            String javafxVersion = System.getProperty("javafx.version");
            Label l = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
            Scene scene = new Scene(l, 640, 480);
        primaryStage.setScene(scene);
        primaryStage.show();
*/

    }

    public static void main(String[] args) {
        launch(args);
    }
}